# hoodie-preview

Quick script to help show what different color combinations would look like.


For the cool hoodies being sold by DOXOlove
https://www.etsy.com/shop/DOXOlove


Based on the excellent game OFF by Mortis Ghost
https://forum.starmen.net/forum/Fan/Games/OFF-by-Mortis-Ghost


![Example Hoodies](./img/screenshot.png)


## Running

Requires a web server like nginx with php

You must install image magick, probably

```
sudo apt install php-imagick
```

May have to reload php server, e.g.:

```
sudo service php7.0-fpm restart
```

The index file has a base url in the script you may have to update too.
